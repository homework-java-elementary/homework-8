package com.company.homework8;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        File file = new File("./");
        List<String> list = filesAtFolder(file);
        saveListToFile(list);
    }

    public static List<String> filesAtFolder(File file){
        List<String> list = new ArrayList<>();
        for (File s : file.listFiles()) {
            if(s.getName()==".." || s.getName() == ".") continue;

            if(s.isFile()) {
                list.add(s.getAbsolutePath());
            }else{
                list.addAll(filesAtFolder(s));
            }
        }
        return list;
    }

    public static void saveListToFile(List<String> list){
        try(OutputStream outputStream = new BufferedOutputStream(new FileOutputStream("List.txt"))){
            for (String s : list) {
                outputStream.write(s.getBytes(StandardCharsets.UTF_8));
                outputStream.write("\n".getBytes(StandardCharsets.UTF_8));
            }
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

    }
}
